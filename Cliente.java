public class Cliente {
    private String nome;
    private int compras;

    public Cliente(String nome, boolean b, int compras) {
        this.nome = nome;
        this.compras = compras;
    }

    public String getNome() {

        return this.nome;
    }


    public int getCompras() {
        return this.compras;
    }

    @Override
    public String toString() {
        return "Cliente:  " + 
                  nome +
    
                ", Compras =  " + compras;
                
    }
}
